from django.apps import AppConfig


class GroupsAppConfig(AppConfig):

    name = "government.groups"
    verbose_name = "Informal Parliamentary Groups"

    def ready(self):
        try:
            import users.signals  # noqa F401
        except ImportError:
            pass
