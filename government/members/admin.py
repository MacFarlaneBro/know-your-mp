from django.contrib import admin
from django.contrib.auth import admin as auth_admin

from government.members.models import Member, Person, Lord, Vote, Election, House
from government.members.forms import MemberChangeForm, MemberCreationForm


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):

    form = MemberChangeForm
    add_form = MemberCreationForm
    # fieldsets = (("Member", {"fields": ("name",)}),)
    # list_display = ["name"]
    # search_fields = ["name"]


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    pass
# fieldsets = (("Member", {"fields": ("name",)}),)
    # list_display = ["name"]
    # search_fields = ["name"]


@admin.register(Lord)
class LordAdmin(admin.ModelAdmin):
    pass


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    pass


@admin.register(Election)
class ElectionAdmin(admin.ModelAdmin):
    pass


@admin.register(House)
class HouseAdmin(admin.ModelAdmin):
    pass
