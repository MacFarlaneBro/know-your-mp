from django.urls import path

from government.members.views import (
    member_list_view,
    member_redirect_view,
    member_update_view,
    member_detail_view,
    cabinet_view
)

app_name = "members"
urlpatterns = [
    path("", view=member_list_view, name="list"),
    path("cabinet/", view=cabinet_view, name="cabinet"),
    path("~redirect/", view=member_redirect_view, name="redirect"),
    path("~update/", view=member_update_view, name="update"),
    path("<slug:slug>/", view=member_detail_view, name="detail"),
]
