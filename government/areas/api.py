from urllib.parse import urlparse

from django.utils.dateparse import parse_datetime

from government.areas.models import Constituency
from government.core.api import load_paginated_url, load_endpoint_url


CONSTITUENCIES = "http://lda.data.parliament.uk/constituencies.json"


def load_constituencies():
    load_paginated_url(CONSTITUENCIES, extract_constituencies)


def extract_constituencies(constituency_page):
    for constituency in constituency_page["items"]:
        try:
            Constituency.objects.get_or_create(
                name=constituency['label']['_value'],
                constituency_type=constituency['constituencyType'],
                ended_date=parse_datetime(constituency["endedDate"]["_value"])
                # previous_constituency
                # date_of_creation
            )
        except Exception as e:
            print(f"poorly formatted constituency: {constituency} - Exception generated: {e}")

    print(Constituency.objects.all())


def load_constituency(constituency_endpoint):
    path = urlparse(constituency_endpoint).path
    constituency_json = load_endpoint_url(path)

    constituency, _ = Constituency.objects.get_or_create(
        name=constituency_json["label"]["_value"],
        constituency_id=path.split("/")[-1]
    )

    if 'constituencyType' in constituency_json:
        constituency.constituency_type = constituency_json["constituencyType"]

    return constituency
