from django.contrib import admin
from .models import Constituency


@admin.register(Constituency)
class ConstituencyAdmin(admin.ModelAdmin):
    pass
